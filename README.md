# Google Summer of Code 2016 #

This repository currently contains source code of jpf-core adopted for mobile (Android) devices.

See the [wiki](https://bitbucket.org/projectkayur/jpf-mobile-devices/wiki/Home) for the project's status, updates, and build instructions.
