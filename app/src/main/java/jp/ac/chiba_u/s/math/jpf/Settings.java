package jp.ac.chiba_u.s.math.jpf;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A singleton class to store app's settings.
 */
public class Settings {
  public static final String EXAMPLES_DIR = Relay.getHomeDir() + "/" + "examples";

  private static final String SETTINGS_FILE = Relay.getHomeDir() + "/" + "settings.json";

  private static final String EXT_LIST_NODE = "extensions";
  //private static final String SINGLE_EXT_NODE = "extension";
  private static final String EXT_NAME_NODE = "ext_name";
  private static final String EXT_ENABLED_NODE = "ext_enabled";

  private static final String JPF_ARGS_NODE = "jpf_args";
  private static final String TARGET_NODE = "target";

  public static final String S_EXT_CORE_NAME = "jpf-core";
  private static final String S_EXT_HNANDLER_NAME = "jpf-nhandler";
  private static final String S_EXT_NETIOCACHE_NAME = "jpf-netiocache";

  private static final Settings sInstance = new Settings();

  private String jpfArgs;
  private String target;
  private List<ExtensionDescriptor> mJpfExtensions = new ArrayList<>();


  private Settings() {
    load();
  }

  public static Settings getInstance() {
    return sInstance;
  }


  private void load() {
    File f = new File(SETTINGS_FILE);

    if (!f.exists() || !f.isFile()) {
      initDefaultSettings();
      return;
    }

    JSONObject root;

    try {
      // read file
      byte[] data = IOUtils.read(new FileInputStream(f));
      String jsonString = new String(data);

      // init JSON structure
      root = new JSONObject(jsonString);

      // read each setting
      jpfArgs = root.getString(JPF_ARGS_NODE);
      target = root.getString(TARGET_NODE);

      //JSONObject extListObj = root.getJSONObject(EXT_LIST_NODE);
      JSONArray extObjects = root.getJSONArray(EXT_LIST_NODE);

      mJpfExtensions.clear();

      for (int i = 0; i < extObjects.length(); i++) {
        JSONObject extObj = extObjects.getJSONObject(i);
        String name = extObj.getString(EXT_NAME_NODE);
        boolean enabled = extObj.getBoolean(EXT_ENABLED_NODE);
        mJpfExtensions.add(new ExtensionDescriptor(name, enabled));
      }

      //
    } catch (Exception e) {
      Relay.out.printf("Cannot read settings file '%s'.\nReason: %s.\n",
          SETTINGS_FILE, e.getMessage());
      initDefaultSettings();
    }
  }

  private void save() {

    try {
      // put settings into a json object
      JSONObject root = new JSONObject();
      root.put(JPF_ARGS_NODE, jpfArgs);
      root.put(TARGET_NODE, target);

      JSONArray extList = new JSONArray();

      for (ExtensionDescriptor d : mJpfExtensions) {
        JSONObject extObj = new JSONObject();
        extObj.put(EXT_NAME_NODE, d.getName());
        extObj.put(EXT_ENABLED_NODE, d.isEnabled());
        extList.put(extObj);
      }

      root.put(EXT_LIST_NODE, extList);

      // convert to string
      String jsonString = root.toString(2);  // arg: indent

      // save into the file
      IOUtils.write(jsonString.getBytes(), SETTINGS_FILE);
      //
    } catch (Exception e) {
      Relay.out.printf("Cannot save settings file '%s'.\nReason: %s.\n",
          SETTINGS_FILE, e.getMessage());
    }
  }

  private void initDefaultSettings() {
    jpfArgs = "-show -log +log.level=info";
    target = "./examples/HelloWorld.jpf";

    mJpfExtensions.clear();
    mJpfExtensions.add(new ExtensionDescriptor(S_EXT_CORE_NAME, true));
    mJpfExtensions.add(new ExtensionDescriptor(S_EXT_HNANDLER_NAME, true));
    mJpfExtensions.add(new ExtensionDescriptor(S_EXT_NETIOCACHE_NAME, false));

    save();
  }

  public String getJpfArgs() {
    return jpfArgs;
  }

  public void setJpfArgs(String val) {
    jpfArgs = val;
    save();
  }

  public String getTarget() { return target; }

  public void setTarget(String target) {
    this.target = target;
    save();
  }

  public List<ExtensionDescriptor> getJpfExtensionList() {
    return mJpfExtensions;
  }

  public void notifyExtensionListChanged() {
    save();
    updateExtensionList();
  }

  public void updateExtensionList() {
    try {
      JpfManager.setExtensionList(mJpfExtensions);
    } catch (IOException e) {
      Relay.out.println(e.getMessage());
    }
  }
}
