package jp.ac.chiba_u.s.math.jpf;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Locale;


public final class RelayPrintStream extends PrintStream {
  private static TextView sOutput = null;
  private static ScrollView sOutputScroll = null;

  private static OutputStream os;
  private static final boolean DEBUG_LOG_FLUSH = false;

  public RelayPrintStream(OutputStream out) {
    super(out);
  }

  public void setOutputWindow(TextView outputWindow, ScrollView scrollView) {
    sOutput = outputWindow;
    sOutputScroll = scrollView;

    if (DEBUG_LOG_FLUSH) {
      try {
        os = new FileOutputStream(Relay.getProperty("user.home") + "/trace.log");
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public PrintStream printf(String format, Object... args) {
    print(String.format(format, args));
    return this;// super.printf(format, args);
  }

  @Override
  public PrintStream printf(Locale l, String format, Object... args) {
    print(String.format(l, format, args));
    return this; //super.printf(l, format, args);
  }

  @Override
  public void print(String str) {
    if (DEBUG_LOG_FLUSH) {
      try {
        os.write(str.getBytes());
        os.flush();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    MessagePollThread.add(str);
    //commit(str);
  }

  public void commit(String str) {
    if (sOutput != null) {
      if (str == null) {
        commit("null");
        return;
      }

      sOutput.append(str);

      if (sOutputScroll != null) {
        sOutputScroll.fullScroll(View.FOCUS_DOWN);
      }

      Log.d("jpf", str);
    }
  }

  @Override
  public void write(byte[] buffer, int offset, int length) {
    print(new String(buffer, offset, length));
  }

  @Override
  public synchronized void write(int oneByte) {
    print(new String(new byte[] {(byte)oneByte}));
  }
}
