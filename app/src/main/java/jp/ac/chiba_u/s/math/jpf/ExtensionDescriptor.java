package jp.ac.chiba_u.s.math.jpf;

/**
 * Holds data on loaded JPF extensions.
 */
public class ExtensionDescriptor {
  private String mName;
  private boolean mEnabled;

  public ExtensionDescriptor(String name, boolean enabled) {
    mName = name;
    mEnabled = enabled;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public boolean isEnabled() {
    return mEnabled;
  }

  public void setEnabled(boolean enabled) {
    mEnabled = enabled;
  }
}
