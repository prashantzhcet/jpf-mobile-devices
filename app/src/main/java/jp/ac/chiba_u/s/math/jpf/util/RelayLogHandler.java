package jp.ac.chiba_u.s.math.jpf.util;

import java.util.logging.LogRecord;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.util.LogHandler;
import jp.ac.chiba_u.s.math.jpf.Relay;

/**
 * Log handler that formats a message and passes it to the Relay.
 */
public final class RelayLogHandler extends LogHandler {
  public RelayLogHandler(Config config) {
    super(config);
  }

  @Override
  public void publish(LogRecord record) {
    if (isLoggable(record))
      Relay.out.println(getFormatter().format(record));
  }
}
