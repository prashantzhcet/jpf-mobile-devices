package jp.ac.chiba_u.s.math.jpf.report;


import java.io.PrintWriter;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.report.ConsolePublisher;
import gov.nasa.jpf.report.Reporter;
import jp.ac.chiba_u.s.math.jpf.Relay;

public class RelayPublisher extends ConsolePublisher {

  public RelayPublisher(Config conf, Reporter reporter) {
    super(conf, reporter);
  }

  @Override
  protected void openChannel() {
    out = new PrintWriter(Relay.out, true);
  }
}
