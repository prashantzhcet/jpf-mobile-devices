package jp.ac.chiba_u.s.math.jpf;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * The main Activity (window) of the app.
 *
 * The method onCreate() can be considered as an entry point
 * of the application.
 */
public class MainActivity extends AppCompatActivity {
  //private static final String DEBUG_ARGS = "-show -log +log.level=info ./examples/HelloWorld.jpf";
  //private static final String DEBUG_ARGS = "-show -log +log.level=finest ./examples/HelloWorld.jpf";
  //private static final String DEBUG_ARGS = "-show -log ./examples/HelloWorld.jpf";
  private static final int REQUEST_CODE_SELECT_TARGET = 0xa;

  private static final int SCROLL_STEP = 50;
  private static final int SCROLL_LONG_PRESS_DELAY = 50;

  private static final String OUTPUT_MSG_EXTRA = "jp.ac.chiba_u.s.math.jpf.MainActivity_msg";
  private static final String OUTPUT_MSG_INTENT_NAME = "jp.ac.chiba_u.s.math.jpf.MainActivity_intent_msg";

  private static Context sContext = null;

  private String jpfTargetPath = "";

  // text field to input jpf args
  private EditText jpfArgsField;
  // text field to display jpf output
  private TextView mFieldOutput;

  private Toolbar toolbar;

  private ScrollView mOutputScrollView;

  private enum ScrollDirection {
    DOWN, BOTTOM, UP, TOP
  }

  private boolean mAutoScroll = false;

  private class ScrollThread extends AsyncTask<Void, Void, Void> {
    ScrollDirection mDirection;

    public ScrollThread(ScrollDirection direction) {
      super();
      mDirection = direction;
    }

    @Override
    protected Void doInBackground(Void... params) {
      while (mAutoScroll) {
        scroll(mDirection);
        try {
          Thread.sleep(SCROLL_LONG_PRESS_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      return null;
    }
  }

  public static void sendMessage(String msg) {
    Intent i = new Intent(OUTPUT_MSG_INTENT_NAME);
    i.putExtra(OUTPUT_MSG_EXTRA, msg);
    LocalBroadcastManager.getInstance(sContext).sendBroadcast(i);
  }

  /* UI and UX related methods */


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    sContext = this;

    // register a broadcast receiver for receiving output messages
    LocalBroadcastManager.getInstance(sContext).registerReceiver(new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        String msg = intent.getStringExtra(OUTPUT_MSG_EXTRA);
        if (msg != null) {
          Relay.out.commit(msg);
        }
      }
    }, new IntentFilter(OUTPUT_MSG_INTENT_NAME));

    // inflate UI from an XML layout file
    setContentView(R.layout.activity_main);

    // prepare toolbar
    toolbar = (Toolbar) findViewById(R.id.main_toolbar);
    if (toolbar != null) {
      //toolbar.setTitle(R.string.toolbar_title);
      setSupportActionBar(toolbar);
      toolbar.setSubtitle(R.string.target_not_set);
    }

    // prepare input and output text areas
    jpfArgsField = (EditText) findViewById(R.id.field_args);
    if (jpfArgsField != null) {
      jpfArgsField.setTypeface(Typeface.MONOSPACE);
      jpfArgsField.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
        @Override
        public void afterTextChanged(Editable s) {
          Settings.getInstance().setJpfArgs(s.toString());
        }
      });
    }

    mFieldOutput = (TextView) findViewById(R.id.field_output);

    // scroll buttons

    mOutputScrollView = (ScrollView) findViewById(R.id.output_scroll_view);

    setScrollButton(R.id.btn_scroll_down, ScrollDirection.DOWN, true);
    setScrollButton(R.id.btn_scroll_to_bottom, ScrollDirection.BOTTOM, false);
    setScrollButton(R.id.btn_scroll_up, ScrollDirection.UP, true);
    setScrollButton(R.id.btn_scroll_to_top, ScrollDirection.TOP, false);

    // init app logic
    init();
  }

  /**
   * Adds mouse click listeners to a UI element that invoke scrolling
   * of the output window.
   *
   * @param id UI element identifier, defined in an XML layout
   * @param direction scrolling direction
   * @param longPress whether to add a listener to a long press (long touch)
   */
  private void setScrollButton(int id, final ScrollDirection direction, boolean longPress) {
    Button scrollButton = (Button) findViewById(id);
    if (scrollButton != null) {
      scrollButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          scroll(direction);
        }
      });

      if (longPress) {
        scrollButton.setOnTouchListener(new View.OnTouchListener() {
          @Override
          public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
              case MotionEvent.ACTION_DOWN:
                mAutoScroll = true;
                new ScrollThread(direction).execute();
                break;
              case MotionEvent.ACTION_UP:
                mAutoScroll = false;
                break;
            }
            return true;
          }
        });
      }
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // close keyboard
    GuiUtils.closeSoftKeyboard(this);

    switch (item.getItemId()) {
      case R.id.action_run_jpf: // start jpf-core

        String argLine = jpfArgsField.getText().toString().trim();

        if (!jpfTargetPath.isEmpty())
          argLine += " " + jpfTargetPath;

        JpfManager.startJPF(this, argLine);

        return true;
      case R.id.action_set_target:
        startActivityForResult(new Intent(this, TargetSelectionActivity.class), REQUEST_CODE_SELECT_TARGET);
        return true;
      case R.id.action_save_log:
        if (mFieldOutput != null) {
          String log = mFieldOutput.getText().toString();
          byte[] logBytes = log.getBytes();

          SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);

          String filename = String.format("%s/%s.%s",
              Relay.getProperty("user.home") + "/" + "log",
              dateFormat.format(new Date()),
              "log");

          try {
            IOUtils.write(logBytes, filename, true);
          } catch (IOException e) {
            Relay.out.println(e.getMessage());
          }
        }
        return true;
      case R.id.action_clear_log:
        if (mFieldOutput != null) {
          new AlertDialog.Builder(this)
              .setIcon(android.R.drawable.ic_dialog_alert)
              .setTitle(R.string.dialog_clear_log_title)
              .setMessage(R.string.dialog_clear_log_confirmation)
              .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  mFieldOutput.setText("");
                }
              })
              .setNegativeButton(R.string.dialog_no, null)
              .show();
        }
        return true;
      case R.id.action_ext_window:
        // open list of JPF extensions
        startActivity(new Intent(this, ExtensionsActivity.class));
        return true;
      case R.id.action_about_window:
        // show 'About' window
        startActivity(new Intent(this, AboutActivity.class));
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // add items to the action bar (if exists)
    getMenuInflater().inflate(R.menu.toolbar, menu);
    return true;
  }

  /* Logic */


  private void init() {
    // configure relay
    Relay.init(mFieldOutput, mOutputScrollView, this);

    // prepare configuration files (site.properties, jpf.properties)
    checkConfigFiles();

    // set default jpf args
    String prev_args = Settings.getInstance().getJpfArgs();
    jpfArgsField.setText(prev_args);

    jpfTargetPath = Settings.getInstance().getTarget();
    setTargetSubtitle(jpfTargetPath);

    //jpfArgsField.setText(DEBUG_ARGS);

    Settings.getInstance().updateExtensionList();
  }

  private void checkConfigFiles() {
    final String path = Relay.getProperty("user.home");

    AssetManager manager = getResources().getAssets();

    try {
      copyAssets(manager, path, "");
    } catch (IOException e) {
      Relay.out.println("There was an error during Asset initialization: " + e.getMessage());
    }
  }

  private void copyAssets(AssetManager manager, String prefix, String path) throws IOException {
    for (String asset : manager.list(path)) {
      String fullAssetPath = path.isEmpty() ? asset : (path + "/" + asset);

      if (!asset.contains(".")) { // a directory
        if (!path.isEmpty() || (!asset.equals("images") && !asset.equals("sounds") && !asset.equals("webkit"))) { // skip pre-defined dirs
          IOUtils.createDir(prefix + "/" + fullAssetPath);
          copyAssets(manager, prefix, fullAssetPath);
        }
      } else { // a file
        String fileName = prefix + "/" + fullAssetPath;
        InputStream stream = manager.open(fullAssetPath);
        IOUtils.write(IOUtils.read(stream), fileName, true);
        stream.close();
      }
    }
  }

  private void scroll(ScrollDirection direction) {
    if (mOutputScrollView != null) {
      int currentPos = mOutputScrollView.getScrollY();

      switch (direction) {
        case DOWN:
          mOutputScrollView.setScrollY(currentPos + SCROLL_STEP);
          break;
        case UP:
          mOutputScrollView.setScrollY(currentPos - SCROLL_STEP);
          break;
        case BOTTOM:
          mOutputScrollView.fullScroll(View.FOCUS_DOWN);
          break;
        case TOP:
          mOutputScrollView.fullScroll(View.FOCUS_UP);
          break;
      }
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode != Activity.RESULT_OK)
      return;

    if (requestCode == REQUEST_CODE_SELECT_TARGET) {
      jpfTargetPath = Settings.getInstance().getTarget();
      setTargetSubtitle(jpfTargetPath);
    }
  }

  private void setTargetSubtitle(String path) {
    int lastSlash = path.lastIndexOf("/");
    String subtitle = (lastSlash >= 0 && lastSlash != path.length() - 1) ?
        path.substring(lastSlash + 1, path.length()) : path;
    toolbar.setSubtitle(subtitle);
  }
}
