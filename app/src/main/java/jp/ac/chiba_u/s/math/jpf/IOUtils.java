package jp.ac.chiba_u.s.math.jpf;

import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains helper functions to work with input / output.
 */
public final class IOUtils {

  private IOUtils() {
  }


  /**
   * Reads data as a byte array from a stream.
   *
   * @param is an input stream
   * @return an array of bytes read from the stream
   * @throws IOException when stream contains no data, when it wasn't possible to read all data,
   *  or when other I/O errors occur
   */
  public static byte[] read(InputStream is) throws IOException {
    int size = is.available();

    if (size == 0)
      throw new IOException("[Utils]: The stream is associated with no data.");

    byte[] buffer = new byte[size];
    int bytesRead = is.read(buffer);

    if (bytesRead != size)
      throw new IOException("[Utils]: Failed to read all data from the stream.");

    return buffer;
  }


  /**
   * Writes a byte array to a file on the device.
   *
   * @param buf a byte array to write
   * @param destPath an absolute or relative path to a file
   * @param overwrite whether to overwrite an existing file
   * @throws IOException
   */
  public static void write(byte[] buf, String destPath, boolean overwrite) throws IOException {
    if (!overwrite && new File(destPath).exists())
      return;

    OutputStream os = new FileOutputStream(destPath);
    os.write(buf, 0, buf.length);
    os.close();
  }

  public static void write(byte[] buf, String destPath) throws IOException {
    write(buf, destPath, true);
  }


  /**
   * Creates a new directory on the device.
   *
   * @param path an absolute or relative path to the directory
   * @return success / failure
   */
  public static boolean createDir(String path) {
    File f = new File(path);
    return f.exists() ? f.isDirectory() : f.mkdir();
  }


  /**
   * Gets regular files with the given file extension, and directories.
   * Returns a sorted list, where directories come first.
   *
   * @param path an absolute or relative path to a directory to scan
   * @param ext the extension of files to get
   * @return a sorted list of directories and files that match the criteria and are located
   *  in the given directory, or {@code null} if the directory is inaccessible
   */
  @Nullable
  public static List<File> getFiles(String path, String ext) {
    File file = new File(path);

    if (!file.exists() || !file.isDirectory())
      return null;

    File[] files = file.listFiles(pathname ->
        pathname.isDirectory() || pathname.getName().endsWith("." + ext)); // file filter

    if (files == null)
      return null; // access denied

    if (files.length == 0)
      return new ArrayList<>(); // no files

    List<File> total = new ArrayList<>();    // contains dirs at first
    List<File> regular = new ArrayList<>();  // contains regular files

    for (File f : files)
      if (f.isDirectory())
        total.add(f);
      else
        regular.add(f);

    Collections.sort(total);
    Collections.sort(regular);

    total.addAll(regular);

    return total;
  }


  /**
   * Gets the parent of the given directory.
   *
   * @param path a path to the directory
   * @return the parent of the given dir
   */
  @Nullable
  public static String getParentDir(String path) {
    if (path.equals("/"))
      return null;

    int start = path.endsWith("/") ? path.length() - 2 : path.length() - 1;
    int index = path.lastIndexOf("/", start);

    return (index != -1) ? path.substring(0, index) : null;
  }

}
