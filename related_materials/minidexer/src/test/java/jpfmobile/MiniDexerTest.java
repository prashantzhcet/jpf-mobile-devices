package jpfmobile;

import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static junit.framework.TestCase.assertNotNull;

public class MiniDexerTest {
    @Test
    public void toDexTest() throws Exception {
        byte[] data = Files.readAllBytes(Paths.get("src/test/java/Main.class"));
        byte[] dexed = MiniDexer.toDex("Main", data);
        assertNotNull(dexed);
        Files.write(Paths.get("src/test/java/Main.dex"), dexed);
    }

    @Test
    public void writeFileTest() throws Exception {
        MiniDexer.writeFile("one/two/three", new byte[] {0xc, 0xa, 0xf, 0xe} );
    }
}
