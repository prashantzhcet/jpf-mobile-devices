package jpfmobile;

import com.android.dx.cf.direct.DirectClassFile;
import com.android.dx.cf.direct.StdAttributeFactory;
import com.android.dx.dex.DexOptions;
import com.android.dx.dex.cf.CfOptions;
import com.android.dx.dex.cf.CfTranslator;
import com.android.dx.dex.file.ClassDefItem;
import com.android.dx.dex.file.DexFile;

import java.io.*;

public final class MiniDexer {
    private MiniDexer() {}

    public static byte[] toDex(String name, byte[] bytes) {

        DexOptions dexOptions = new DexOptions();
        DexFile outputDex = new DexFile(dexOptions);
        CfOptions cfOptions = new CfOptions();

        DirectClassFile cf = new DirectClassFile(bytes, name, false);
        cf.setAttributeFactory(StdAttributeFactory.THE_ONE);
        cf.getMagic(); // triggers the actual parsing

        ClassDefItem cdif = CfTranslator.translate(cf, bytes, cfOptions,
                dexOptions, outputDex);
        outputDex.add(cdif);

        byte[] outArray; // this array is null if no classes were defined

        if (outputDex.isEmpty()) {
            throw new RuntimeException("[MiniDexer] Output DEX is empty.");
        }

        try {
            outArray = outputDex.toDex(null, false);
        } catch (IOException e) {
            throw new RuntimeException("[MiniDexer] DexFile.toDex() failure: " + e.getMessage(), e);
        }

        if (outArray == null) {
            throw new RuntimeException("[MiniDexer] DexFile.toDex() failure: null is returned.");
        }

        return outArray;
    }

    public static void toDexFile(String className, String inputFile, String outputFile) {
        try {
            byte[] data = readFile(inputFile);
            toDexFile(className, data, outputFile);
        } catch (Exception e) {
            throw new RuntimeException("[MiniDexer] File I/O failure: " + e.getMessage(), e);
        }
    }

    public static void toDexFile(String className, byte[] data, String outputFile) {

        byte[] dexed = MiniDexer.toDex(className, data);

        try {
            writeFile(outputFile, dexed);
        } catch (Exception e) {
            throw new RuntimeException("[MiniDexer] File I/O failure: " + e.getMessage(), e);
        }
    }

    static byte[] readFile(String filename) throws IOException {
        // Java 7: return Files.readAllBytes(Paths.get(filename));

        File file = new File(filename);
        byte[] data = new byte[(int) file.length()];

        FileInputStream fs = null;

        try {
            fs = new FileInputStream(file);
            int readBytes = fs.read(data, 0, data.length);
            if (readBytes != data.length) {
                throw new IOException("File was not completely read.");
            }
        } finally {
            if (fs != null) fs.close();
        }

        return data;
    }

    static void writeFile(String filename, byte[] data) throws IOException {
        // Java 7: Files.write(Paths.get(filename), data);

        System.out.println("Writing file " + filename);

        if (filename.contains("/")) {
            String[] dirs = filename.split("/");
            String path = filename.startsWith("/") ? "/" : "";

            for (int i = 0; i < dirs.length - 1; i++) {
                if (dirs[i].trim().isEmpty())
                    continue;

                path += dirs[i] + "/";
                File f = new File(path);

                if (!f.exists()) {
                    boolean result = f.mkdir();
                    if (!result) {
                        throw new IOException("Cannot create dir: " + path);
                    }
                }
            }
        }

        File file = new File(filename);
        FileOutputStream fs = null;

        try {
            fs = new FileOutputStream(file);
            fs.write(data, 0, data.length);
        } finally {
            if (fs != null) fs.close();
        }
    }
}
